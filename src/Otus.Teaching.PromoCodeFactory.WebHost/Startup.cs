using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
//using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.EFconfig;
using Otus.Teaching.PromoCodeFactory.Implementation;
using Otus.Teaching.PromoCodeFactory.WebHost.DI;
using Otus.Teaching.PromoCodeFactory.WebHost.Settings;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();


            services
                .AddSingleton((IConfigurationRoot)this.Configuration)
                .AddDbContext<DatabaseContext>(optionsBuilder
                => optionsBuilder.UseSqlite(this.Configuration.Get<ApplicationSettings>().ConnectionString));

            services.AddDI();

            services.AddAutoMapper(typeof(AppMappingProfile));
            services.AddAutoMapper(typeof(Implementation.Mapper.AppMappingProfile));

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var db = serviceScope.ServiceProvider.GetRequiredService<DatabaseContext>();
                db.Database.EnsureDeleted();
                db.Database.EnsureCreated();
                serviceScope.ServiceProvider.GetRequiredService<IService<Role, IRoleRepo>>().FillTable();
                serviceScope.ServiceProvider.GetRequiredService<IService<Employee, IEmployeeRepo>>().FillTable();
                serviceScope.ServiceProvider.GetRequiredService<IService<Preference, IPreferenceRepo>>().FillTable();
                serviceScope.ServiceProvider.GetRequiredService<IService<PromoCode, IPromoCodeRepo>>().FillTable();
                serviceScope.ServiceProvider.GetRequiredService<IService<Customer, ICustomerRepo>>().FillTable();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}