﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IService<PromoCode, IPromoCodeRepo> promoCodeRepository;
        private readonly IService<Employee, IEmployeeRepo> employeeRepository;
        private readonly IService<Customer, ICustomerRepo> customerRepository;
        private readonly IService<Preference, IPreferenceRepo> preferenceCodeRepository;

        public PromocodesController(
            IService<PromoCode, IPromoCodeRepo> promoCodeRepository,
            IService<Preference, IPreferenceRepo> preferenceCodeRepository,
            IService<Employee, IEmployeeRepo> employeeRepository,
            IService<Customer, ICustomerRepo> customerRepository)
        {
            this.promoCodeRepository = promoCodeRepository;
            this.employeeRepository = employeeRepository;
            this.preferenceCodeRepository = preferenceCodeRepository;
            this.customerRepository = customerRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await this.promoCodeRepository.GetAllAsync();

            var promoCodesModelList = promoCodes.Select(x =>
                new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    Code = x.Code,
                    ServiceInfo = x.ServiceInfo,
                    BeginDate = x.BeginDate.ToString(),
                    EndDate = x.EndDate.ToString(),
                    PartnerName = x.PartnerName

                }).ToList();

            return promoCodesModelList;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = this.preferenceCodeRepository.GetAll().First(x => x.Name == request.Preference);

            if(preference == null)
            {
                return BadRequest(request);
            }

            var manager = this.employeeRepository.GetAll().FirstOrDefault();
            var promoCode = new PromoCode() 
            { 
                PartnerName = request.PartnerName,
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                Preference = preference,
                PartnerManager = manager,
                Id = Guid.NewGuid(),
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddYears(1)
            };

            await this.promoCodeRepository.Create(promoCode);

            var customers = this.customerRepository.GetAllWithIncluded()
                .Where(x=>x.Preferences.Select(x=>x.Id).Contains(preference.Id))
                .ToList();
            customers.ForEach(x =>
            {
                x.PromoCodes.Add(this.promoCodeRepository.GetById(promoCode.Id));
                this.customerRepository.Update(x);
            });

            return Ok(promoCode);
        }
    }
}