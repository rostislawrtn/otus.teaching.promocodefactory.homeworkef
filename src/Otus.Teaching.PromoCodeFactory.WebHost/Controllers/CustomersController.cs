﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;

using Microsoft.AspNetCore.Mvc;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.DTO;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IService<Customer, ICustomerRepo> customerRepository;
        public CustomersController(
            IService<Customer, ICustomerRepo> customerRepository,
            IMapper mapper)
        {
            this.mapper = mapper;
            this.customerRepository = customerRepository;
        }

        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await this.customerRepository.GetAllAsync();
            return Ok(customers.Select(x=> this.mapper.Map<CustomerShortResponse>(x)));
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await this.customerRepository.GetByIdAsync(id);
            return Ok(this.mapper.Map<CustomerResponse>(customer));
        }
        
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {

            var customer = this.mapper.Map<CustomerDto>(request);

            await this.customerRepository.Create(customer);
            return Ok();

        }
        
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            if (!this.validateCustomer(id))
            {
                return BadRequest();
            }

            var customer = this.mapper.Map<CustomerDto>(request);
            await this.customerRepository.Update(id, customer);
            return Ok();
        }
        
        [HttpDelete]
        public IActionResult DeleteCustomer(Guid id)
        {
            if (!this.validateCustomer(id))
            {
                return BadRequest();
            }

            this.customerRepository.Remove(id);
            return Ok();
        }

        private bool validateCustomer(Guid id)
        {
            return this.customerRepository.GetById(id) != null;
        }
    }
}