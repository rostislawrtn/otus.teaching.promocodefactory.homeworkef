﻿using Microsoft.Extensions.DependencyInjection;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.Implementation;

namespace Otus.Teaching.PromoCodeFactory.WebHost.DI;

public static class Configs
{
    public static IServiceCollection AddDI(this IServiceCollection services)
    {
        services.AddScoped(typeof(IMemoryRepository<Employee>), (x) =>
            new InMemoryRepository<Employee>(FakeDataFactory.Employees));
        services.AddScoped(typeof(IMemoryRepository<Role>), (x) =>
            new InMemoryRepository<Role>(FakeDataFactory.Roles));
        services.AddScoped(typeof(IMemoryRepository<Preference>), (x) =>
            new InMemoryRepository<Preference>(FakeDataFactory.Preferences));
        services.AddScoped(typeof(IMemoryRepository<Customer>), (x) =>
            new InMemoryRepository<Customer>(FakeDataFactory.Customers));
        services.AddScoped(typeof(IMemoryRepository<PromoCode>), (x) =>
            new InMemoryRepository<PromoCode>(FakeDataFactory.PromoCodes));

        services.AddScoped<IService<Role, IRoleRepo>, RoleService>();
        services.AddScoped<IRoleRepo, RoleRepository>();

        services.AddScoped<IService<Employee, IEmployeeRepo>, EmployeeService>();
        services.AddScoped<IEmployeeRepo, EmployeeRepository>();

        services.AddScoped<IService<PromoCode, IPromoCodeRepo>, PromoCodeService>();
        services.AddScoped<IPromoCodeRepo, PromoCodeRepository>();

        services.AddScoped<IService<Preference, IPreferenceRepo>, PreferenceService>();
        services.AddScoped<IPreferenceRepo, PreferenceRepository>();

        services.AddScoped<IService<Customer, ICustomerRepo>, CustomerService>();
        services.AddScoped<ICustomerRepo, CustomerRepository>();

        return services;
    }
}
