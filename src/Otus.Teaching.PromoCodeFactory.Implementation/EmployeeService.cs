﻿using Microsoft.EntityFrameworkCore;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Implementation;
public class EmployeeService : IService<Employee, IEmployeeRepo>
{
    private readonly IEmployeeRepo repo;
    private readonly IService<Role, IRoleRepo> roleRepo;
    public EmployeeService(
        IEmployeeRepo repo,
        IMemoryRepository<Employee> memoryRepo,
        IService<Role, IRoleRepo> roleRepo)
        : base(repo, memoryRepo)
    {
        this.repo = repo;  
        this.roleRepo = roleRepo;
    }

    public override async Task<Employee> Create(Employee obj)
    {
        var newEmployee = new Employee()
        {
            Id = obj.Id,
            FirstName = obj.FirstName,
            LastName = obj.LastName,
            Email = obj.Email,
            Role = this.roleRepo.GetByIdAsync(obj.Role.Id).Result ?? obj.Role,
            AppliedPromocodesCount = obj.AppliedPromocodesCount
        };
        var res = await this.repo.AddAsync(newEmployee);
        await this.repo.SaveChangesAsync();
        return res;
    }

    public override async Task<Employee> GetByIdAsync(Guid id)
    {
        return this.repo.GetAll()
            .Include(x => x.Role)
            .First(x => x.Id == id);
    }
}

