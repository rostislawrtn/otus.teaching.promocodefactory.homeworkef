﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Implementation;
public class PreferenceService : IService<Preference, IPreferenceRepo>
{
    private readonly IPreferenceRepo repo;
    private readonly IService<Customer, ICustomerRepo> customerRepo;

    public PreferenceService(
        IPreferenceRepo repo,
        IMemoryRepository<Preference> memoryRepo)
        : base(repo, memoryRepo)
    {
        this.repo = repo;
    }
    
    public override async Task<Preference> Create(Preference obj)
    {
        var newCustomer = new Preference()
        {
            Id = obj.Id,
            Name = obj.Name
        };
        var res = await this.repo.AddAsync(newCustomer);
        await this.repo.SaveChangesAsync();
        return res;
    }
    private Customer CheckCustomer(Customer customer)
    {
        return this.customerRepo.GetById(customer.Id) ?? customer;
    }

}