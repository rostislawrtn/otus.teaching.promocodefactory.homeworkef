﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Implementation;
public class PromoCodeService : IService<PromoCode, IPromoCodeRepo>
{
    private readonly IPromoCodeRepo repo;
    private readonly IService<Preference, IPreferenceRepo> preferenceRepo;
    private readonly IService<Employee, IEmployeeRepo> employeeRepo;

    public PromoCodeService(
        IPromoCodeRepo repo,
        IMemoryRepository<PromoCode> memoryRepo,
        IService<Preference, IPreferenceRepo> preferenceRepo,
        IService<Employee, IEmployeeRepo> employeeRepo)
        : base(repo, memoryRepo)
    {
        this.repo = repo;
        this.preferenceRepo = preferenceRepo;
        this.employeeRepo = employeeRepo;
    }

    public override async Task<PromoCode> Create(PromoCode obj)
    {
        var newPromoCode = new PromoCode()
        {
            Id = obj.Id,
            Code = obj.Code,
            ServiceInfo = obj.ServiceInfo,
            BeginDate = obj.BeginDate,
            EndDate = obj.EndDate,
            PartnerName = obj.PartnerName,
            PartnerManager = this.employeeRepo.GetById(obj.PartnerManager.Id) ?? obj.PartnerManager,
            Preference = this.preferenceRepo.GetById(obj.Preference.Id) ?? obj.Preference
        };
        var res = await this.repo.AddAsync(newPromoCode);
        await this.repo.SaveChangesAsync();
        return res;
    }
}