﻿using AutoMapper;

using Microsoft.EntityFrameworkCore;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Implementation;
public class CustomerService : IService<Customer, ICustomerRepo>
{
    private readonly IMapper mapper;
    private readonly ICustomerRepo repo;
    private readonly IService<Preference, IPreferenceRepo> preferenceRepo;
    private readonly IService<PromoCode, IPromoCodeRepo> promoCodeRepo;
    public CustomerService(
        ICustomerRepo repo,
        IMemoryRepository<Customer> memoryRepo,
        IService<Preference, IPreferenceRepo> preferenceRepo,
        IService<PromoCode, IPromoCodeRepo> promoCodeRepo,
        IMapper mapper)
        : base(repo, memoryRepo)
    {
        this.mapper = mapper;
        this.repo = repo;
        this.preferenceRepo = preferenceRepo;
        this.promoCodeRepo = promoCodeRepo;
    }

    public override async Task<Customer> Create(Customer obj)
    {
        var newCustomer = new Customer()
        {
            Id = obj.Id,
            FirstName = obj.FirstName,
            LastName = obj.LastName,
            Email = obj.Email,
            Preferences = obj.Preferences.Select(this.CheckPreference).ToList(),
            PromoCodes = obj.PromoCodes.Select(this.CheckPromocode).ToList(),
        };
        var res = await this.repo.AddAsync(newCustomer);
        await this.repo.SaveChangesAsync();
        return res;
    }

    public override async Task<Customer> GetByIdAsync(Guid id)
    {
        return this.repo.GetAll()
            .AsNoTracking()
            .Include(x => x.PromoCodes)
            .Include(x => x.Preferences)
            .First(x=>x.Id == id);
    }

    public override async Task Update<CustomerDto>(Guid id, CustomerDto obj)
    {
        var updatedCustomer = this.mapper.Map<Customer>(obj);
        var customer = this.repo.GetAll()
            .Include(x => x.PromoCodes)
            .Include(x => x.Preferences)
            .First(x => x.Id == id);
        updatedCustomer.Id = id;
        updatedCustomer.Preferences = (obj as Core.DTO.CustomerDto).PreferenceIds
            .Select(x => this.preferenceRepo.GetById(x)).ToList();
        this.repo.Update(updatedCustomer);
        await this.repo.SaveChangesAsync();
    }

    public override async Task Create<CustomerDto>(CustomerDto obj)
    {
        var customer = 
            this.mapper.Map<CustomerDto ,Customer>(obj);
        customer.Preferences = (obj as Core.DTO.CustomerDto).PreferenceIds
            .Select(x=>this.preferenceRepo.GetById(x)).ToList();
        await this.repo.AddAsync(customer);
        await this.repo.SaveChangesAsync();
    }

    public override void Remove(Guid id)
    {
        this.repo.Remove(id);
        this.repo.SaveChanges();
    }

    public override IQueryable<Customer> GetAllWithIncluded()
    {
        return this.repo.GetAll()
            .Include(x => x.PromoCodes)
            .Include(x => x.Preferences);
    }

    private Preference CheckPreference(Preference preference)
    {
        return this.preferenceRepo.GetById(preference.Id) ?? preference;
    }

    private PromoCode CheckPromocode(PromoCode promoCode)
    {
        return this.promoCodeRepo.GetById(promoCode.Id) ?? promoCode;
    }    
}
