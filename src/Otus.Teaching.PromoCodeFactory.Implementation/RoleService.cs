﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Implementation;
public class RoleService : IService<Role, IRoleRepo>
{
    public RoleService(
        IRoleRepo repo,
        IMemoryRepository<Role> memoryRepo)
        : base(repo, memoryRepo)
    {
    }
}

