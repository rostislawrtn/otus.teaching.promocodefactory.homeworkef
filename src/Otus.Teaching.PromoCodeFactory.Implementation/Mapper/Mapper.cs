﻿using AutoMapper;

using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.DTO;

namespace Otus.Teaching.PromoCodeFactory.Implementation.Mapper;

public class AppMappingProfile : Profile
{
    public AppMappingProfile()
    {
        CreateMap<Preference, Guid>()
            .ConstructUsing(src => src.Id);

        CreateMap<Guid, Preference>()
            .ConstructUsing(src => new Preference { Id = src });

        CreateMap<Customer, CustomerDto>()
                .ForMember(x => x.PreferenceIds, opt => opt.Ignore())
                .ReverseMap();

        //CreateMap<Customer, Customer>()
        //        .ForMember(x => x.Preferences, opt => opt.Ignore())
        //        .ForMember(x => x.PromoCodes, opt => opt.Ignore());


        //CreateMap<CustomerDto, Customer>()
        //    .ForMember(x => x.Preferences, opt => opt.Ignore());
        //CreateMap<CustomerDto, Customer>()
        //    .ForMember(x => x.Preferences, y => y.MapFrom(x => x.PreferenceIds));
        //CreateMap<Guid, Preference>().ForMember(x => x.Id, y => y.MapFrom(x => x));
    }
}