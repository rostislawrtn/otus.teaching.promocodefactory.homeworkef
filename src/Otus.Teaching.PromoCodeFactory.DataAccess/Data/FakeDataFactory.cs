﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                Role = Roles.FirstOrDefault(x => x.Name == "Admin"),
                AppliedPromocodesCount = 8
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                Role = Roles.FirstOrDefault(x => x.Name == "PartnerManager"),
                AppliedPromocodesCount = 12
            },
            new Employee()
            {
                Id = Guid.NewGuid(),
                Email = "test1@somemail.ru",
                FirstName = "test1",
                LastName = "test1",
                Role = Roles.FirstOrDefault(x => x.Name == "User"),
                AppliedPromocodesCount = 2
            },
            new Employee()
            {
                Id = Guid.NewGuid(), 
                Email = "test2@somemail.ru",
                FirstName = "test2",
                LastName = "test2",
                Role = Roles.FirstOrDefault(x => x.Name == "User"),
                AppliedPromocodesCount = 2
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            },
            new Role()
            {
                Id = Guid.NewGuid(),
                Name = "User",
                Description = "User"
            }
        };
        
        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };

        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
                var _preferences = Preferences.Where(x => x.Name != "Театр").ToList();
                var _promoCodes = PromoCodes.Where(x => x.Preference.Name != "Театр").ToList();
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = customerId,
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        Preferences = _preferences,
                        PromoCodes = _promoCodes
                    },
                    new Customer()
                    {
                        Id = Guid.NewGuid(),
                        Email = "test1@mail.ru",
                        FirstName = "test1",
                        LastName = "test1",
                        Preferences = Preferences.Where(x => x.Name == "Театр").ToList(),
                        PromoCodes = PromoCodes.Where(x => x.Preference.Name == "Театр").ToList()
                    }
                };

                return customers;
            }
        }
        public static IEnumerable<PromoCode> PromoCodes
        {
            get
            {
                var promoCodes = new List<PromoCode>()
                {
                    new PromoCode()
                    {
                        Id = Guid.Parse("b2252037-7a8e-4bb4-859c-2c3e152f15b4"),
                        Code = "test1",
                        ServiceInfo = "test1 promo",
                        BeginDate = DateTime.Now.AddDays(-30),
                        EndDate = DateTime.Now.AddDays(30),
                        PartnerName = "Partner 1",
                        PartnerManager = Employees.FirstOrDefault(x => x.Role.Name == "PartnerManager"),
                        Preference = Preferences.FirstOrDefault(x => x.Name =="Театр")
                    },
                    new PromoCode()
                    {
                        Id = Guid.Parse("8954c527-32a2-4c2d-927f-322dfed526b2"),
                        Code = "test2",
                        ServiceInfo = "test2 promo",
                        BeginDate = DateTime.Now.AddDays(-30),
                        EndDate = DateTime.Now.AddDays(30),
                        PartnerName = "Partner 2",
                        PartnerManager = Employees.FirstOrDefault(x => x.Role.Name == "PartnerManager"),
                        Preference = Preferences.FirstOrDefault(x => x.Name =="Семья")
                    },
                    new PromoCode()
                    {
                        Id = Guid.Parse("cfffd8d3-c771-47ac-8a4a-afd9ca2aa781"),
                        Code = "test3",
                        ServiceInfo = "test3 promo",
                        BeginDate = DateTime.Now.AddDays(-30),
                        EndDate = DateTime.Now.AddDays(30),
                        PartnerName = "Partner 3",
                        PartnerManager = Employees.FirstOrDefault(x => x.Role.Name == "PartnerManager"),
                        Preference = Preferences.FirstOrDefault(x => x.Name =="Дети")
                    }
                };
                return promoCodes;
            }
        }
    }
}