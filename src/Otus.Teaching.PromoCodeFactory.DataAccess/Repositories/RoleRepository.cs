﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.EFconfig;
using Otus.Teaching.PromoCodeFactory.Implementation;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
public class RoleRepository : EfRepository<Role>, IRoleRepo
{
    public RoleRepository(DatabaseContext context) : base(context)
    {
    }
}
