﻿using System.Linq;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.EFconfig;

namespace Otus.Teaching.PromoCodeFactory.Implementation;

public class EmployeeRepository : EfRepository<Employee>, IEmployeeRepo
{
    public EmployeeRepository(DatabaseContext context) : base(context)
    {
    }
}
