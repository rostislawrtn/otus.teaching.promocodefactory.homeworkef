﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.EFconfig;
using Otus.Teaching.PromoCodeFactory.Implementation;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
public class PromoCodeRepository : EfRepository<PromoCode>, IPromoCodeRepo
{
    public PromoCodeRepository(DatabaseContext context) : base(context)
    {

    }
}