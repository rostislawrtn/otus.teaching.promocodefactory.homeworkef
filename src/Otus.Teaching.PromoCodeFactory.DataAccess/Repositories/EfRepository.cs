﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Implementation;
public abstract class EfRepository<T> : IEfRepository<T> where T
    : BaseEntity
{
    protected readonly DbContext Context;
    protected readonly DbSet<T> EntitySet;

    protected EfRepository(DbContext context)
    {
        Context = context;
        this.EntitySet = this.Context.Set<T>();
    }

    public async Task<List<T>> GetAllAsync(CancellationToken cancellationToken = default)
    {
        return await GetAll().ToListAsync(cancellationToken);
    }

    public IQueryable<T> GetAll()
    {
        return this.EntitySet;
    }

    public T Get(Guid id)
    {
        return this.EntitySet.Find(id);
    }

    public async Task<T> GetAsync(Guid id)
    {
        return await this.EntitySet.FindAsync((object)id);
    }

    public virtual async Task<T> AddAsync(T entity)
    {
        return (await this.EntitySet.AddAsync(entity)).Entity;
    }

    public virtual void SaveChanges()
    {
        Context.SaveChanges();
    }

    public virtual async Task SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        await Context.SaveChangesAsync(cancellationToken);
    }

    public virtual void Update(T entity)
    {
        Context.Entry(entity).State = EntityState.Modified;
    }

    public virtual void Remove(Guid id)
    {
        this.EntitySet.Remove(this.Get(id));
    }
}

