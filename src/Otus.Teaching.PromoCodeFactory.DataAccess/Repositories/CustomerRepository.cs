﻿using System;
using System.Linq;

using Microsoft.EntityFrameworkCore;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.EFconfig;
using Otus.Teaching.PromoCodeFactory.Implementation;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class CustomerRepository : EfRepository<Customer>, ICustomerRepo
{
    public CustomerRepository(DatabaseContext context) : base(context)
    {
    }

    public override void Update(Customer entity)
    {
        var customer = this.EntitySet
            .Include(x => x.PromoCodes)
            .Include(x => x.Preferences)
            .First(x => x.Id == entity.Id);

        customer.LastName = entity.LastName;
        customer.FirstName = entity.FirstName;
        customer.Email = entity.Email;
        customer.Preferences = entity.Preferences;
        customer.PromoCodes = entity.PromoCodes;

        Context.Entry(customer).State = EntityState.Modified;

        Context.Update(customer);
    }

    public override void Remove(Guid id)
    {
        var customer = this.EntitySet
            .Include(x => x.PromoCodes)
            .Include(x => x.Preferences)
            .First(x => x.Id == id);
        this.EntitySet.Remove(customer);
    }
}