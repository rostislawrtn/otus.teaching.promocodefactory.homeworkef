﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.EFconfig;
using Otus.Teaching.PromoCodeFactory.Implementation;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class PreferenceRepository : EfRepository<Preference>, IPreferenceRepo
{
    public PreferenceRepository(DatabaseContext context) : base(context)
    {
    }
}