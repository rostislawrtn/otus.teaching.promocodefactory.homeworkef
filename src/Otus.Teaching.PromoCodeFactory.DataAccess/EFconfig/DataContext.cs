﻿using System;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EFconfig
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Employee> Employee { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Preference> Preference { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<PromoCode> PromoCode { get; set; }

        public DatabaseContext(
            DbContextOptions<DatabaseContext> options) 
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Customer>()
                    .HasMany(c => c.Preferences)
                    .WithMany()
                    .UsingEntity("CustomerPreference");

            modelBuilder.Entity<Customer>()
                    .HasMany(c => c.PromoCodes)
                    .WithOne();

            modelBuilder.Entity<Employee>().Property(x => x.FirstName).HasMaxLength(50);
            modelBuilder.Entity<Employee>().Property(x => x.LastName).HasMaxLength(50);
            modelBuilder.Entity<Employee>().Property(x => x.Email).HasMaxLength(50);

            modelBuilder.Entity<Role>().Property(x => x.Name).HasMaxLength(50);
            modelBuilder.Entity<Role>().Property(x => x.Description).HasMaxLength(100);

            modelBuilder.Entity<Preference>().Property(x => x.Name).HasMaxLength(100);

            modelBuilder.Entity<Customer>().Property(x => x.FirstName).HasMaxLength(50);
            modelBuilder.Entity<Customer>().Property(x => x.LastName).HasMaxLength(50);
            modelBuilder.Entity<Customer>().Property(x => x.Email).HasMaxLength(50);

            modelBuilder.Entity<PromoCode>().Property(x => x.Code).HasMaxLength(50);
            modelBuilder.Entity<PromoCode>().Property(x => x.ServiceInfo).HasMaxLength(50);
            modelBuilder.Entity<PromoCode>().Property(x => x.PartnerName).HasMaxLength(50);

        }
    }
}
