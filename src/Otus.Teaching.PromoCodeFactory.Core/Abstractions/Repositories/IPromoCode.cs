﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
public interface IPromoCodeRepo : IEfRepository<PromoCode>
{
}