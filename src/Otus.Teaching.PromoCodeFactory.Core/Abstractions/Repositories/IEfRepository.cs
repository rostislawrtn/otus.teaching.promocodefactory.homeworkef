﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
public interface IEfRepository<T>
    where T : BaseEntity
{

    Task<List<T>> GetAllAsync(CancellationToken cancellationToken = default);
    IQueryable<T> GetAll();
    T Get(Guid id);
    Task<T> GetAsync(Guid id);
    Task<T> AddAsync(T entity);
    void SaveChanges();
    Task SaveChangesAsync(CancellationToken cancellationToken = default);
    void Update(T entity);
    void Remove(Guid id); 
}