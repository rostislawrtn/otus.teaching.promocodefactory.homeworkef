﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
public interface IEmployeeRepo : IEfRepository<Employee>
{
}
