﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

public abstract class IService<T, R> 
    where T : BaseEntity 
    where R : IEfRepository<T>
{
    private readonly R repo;
    private readonly IMemoryRepository<T> memoryRepo;

    public IService(
        R repo,
        IMemoryRepository<T> memoryRepo)
    {
        this.repo = repo; 
        this.memoryRepo = memoryRepo;        
    }

    public virtual async Task<T> Create(T obj)
    {
        var res = await this.repo.AddAsync(obj);
        await this.repo.SaveChangesAsync();
        return res;
    }

    public virtual async Task<List<T>> AddList(List<T> obj)
    {
        List<T> resList = new List<T>();

        obj.ForEach(async x =>
        {
            resList.Add(await this.Create(x));
        });

        return resList;
    }

    public virtual async Task<List<T>> GetAllAsync()
    {
        return await this.repo.GetAllAsync();
    }

    public virtual async Task<T> GetByIdAsync(Guid id)
    {
        return await this.repo.GetAsync(id);
    }

    public virtual IQueryable<T> GetAll()
    {
        return this.repo.GetAll();
    }
    
    public virtual IQueryable<T> GetAllWithIncluded()
    {
        return this.repo.GetAll();
    }
    
    public virtual T GetById(Guid id)
    {
        return this.repo.Get(id);
    }

    public virtual async Task Update<DTO>(Guid id, DTO obj)
    {
        throw new NotImplementedException();
    }    

    public virtual void Update(T obj)
    {
        this.repo.Update(obj);
        this.repo.SaveChanges();
    }    

    public virtual async Task Create<DTO>(DTO obj)
    {
        await this.Create(obj as T);
    }    

    public virtual async void FillTable()
    {
        await this.AddList(this.memoryRepo.GetAllAsync().Result.ToList());
    }

    public virtual void Remove(Guid id)
    {
        this.repo.Remove(id);
        this.repo.SaveChanges();
    }
}