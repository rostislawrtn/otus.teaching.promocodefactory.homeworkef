﻿using System.Collections.Generic;
using System;

namespace Otus.Teaching.PromoCodeFactory.Core.DTO;
public abstract record RecordMarker;
public record CustomerDto(string FirstName, string LastName, string Email, List<Guid> PreferenceIds) : RecordMarker;
